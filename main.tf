# Configure the Microsoft Azure provider
provider "azurerm" {
  features {}
}

# Create a Resource Group if it doesn’t exist
resource "azurerm_resource_group" "azuretp1" {
  name     = "eps-tp1-rg"
  location = "West Europe"
}

# Create a Virtual Network
resource "azurerm_virtual_network" "azuretp1" {
  name                = "eps-tp1-vnet"
  location            = azurerm_resource_group.azuretp1.location
  resource_group_name = azurerm_resource_group.azuretp1.name
  address_space       = ["10.0.0.0/16"]
}

# Create a Subnet in the Virtual Network
resource "azurerm_subnet" "azuretp1" {
  name                 = "eps-tp1-subnet"
  resource_group_name  = azurerm_resource_group.azuretp1.name
  virtual_network_name = azurerm_virtual_network.azuretp1.name
  address_prefixes     = ["10.0.2.0/24"]
}

# Create a Network Interface
resource "azurerm_network_interface" "azuretp1" {
  name                = "eps-tp1-nic"
  location            = azurerm_resource_group.azuretp1.location
  resource_group_name = azurerm_resource_group.azuretp1.name

  ip_configuration {
    name                          = "eps-tp1-nic-ip-config"
    subnet_id                     = azurerm_subnet.azuretp1.id
    private_ip_address_allocation = "Dynamic"
  }
}

# Create a Virtual Machine
resource "azurerm_linux_virtual_machine" "azuretp1" {
  name                            = "eps-tp1-vm"
  location                        = azurerm_resource_group.azuretp1.location
  resource_group_name             = azurerm_resource_group.azuretp1.name
  network_interface_ids           = [azurerm_network_interface.azuretp1.id]
  size                            = "Standard_B1s"
  computer_name                   = "eps-vm"
  admin_username                  = "azureuser"
  admin_password                  = "Password1234!"
  disable_password_authentication = false

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    name                 = "eps-tp1-os-disk"
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
}
